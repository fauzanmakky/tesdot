package com.fauzan.tesdot.view.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.fauzan.tesdot.R;
import com.fauzan.tesdot.model.ModelGambar;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MediaAdapter extends RecyclerView.Adapter<MediaAdapter.MediaViewHolder> {
    List<ModelGambar.Media> mediaList;

    public MediaAdapter(List<ModelGambar.Media> media) {
        this.mediaList = media;
    }

    @NonNull
    @Override
    public MediaViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gambar, viewGroup, false);
        return new MediaViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MediaViewHolder mediaViewHolder, int i) {
        Picasso.get().load(mediaList.get(i).getUrl()).into(mediaViewHolder.gambar);
    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    public class MediaViewHolder extends RecyclerView.ViewHolder {
        ImageView gambar;

        public MediaViewHolder(@NonNull View itemView) {
            super(itemView);
            gambar = itemView.findViewById(R.id.item_gambar_gambar);
        }
    }
}
