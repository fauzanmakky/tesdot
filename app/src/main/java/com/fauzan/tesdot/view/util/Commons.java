package com.fauzan.tesdot.view.util;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

public class Commons {
    private static Commons isInstance;

    public static Commons getInstance() {
        if (isInstance == null) {
            isInstance = new Commons();
        }
        return isInstance;
    }

    public Snackbar showSnackbarBottom(View view, String msg) {
        return Snackbar.make(view, msg, Snackbar.LENGTH_LONG);
    }

    public Toast Toast(Context context, String msg) {
        return Toast.makeText(context, msg, Toast.LENGTH_SHORT);
    }

    public boolean isConnection(Context context) {
        ConnectivityManager cm;
        cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        assert cm != null;
        NetworkInfo info = cm.getActiveNetworkInfo();
        return info != null && info.isConnected();
    }
}
