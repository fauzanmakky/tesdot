package com.fauzan.tesdot.view.activity;

import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;

import com.fauzan.tesdot.R;

import com.fauzan.tesdot.model.ModelGambar;
import com.fauzan.tesdot.service.BaseApi;
import com.fauzan.tesdot.view.adapter.ListGambarAdapter;
import com.fauzan.tesdot.view.util.Commons;

import java.util.List;

public class ListGambar extends AppCompatActivity implements ListGambarContract.ListGambarView {
    ListGambarPresenterImp mPresenter;
    ListGambarAdapter listGambarAdapter;
    RecyclerView listGAmbar;
    ConstraintLayout parentLayout;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_gambar);
        setUi();
        BaseApi factory = BaseApi.Factory.create();
        mPresenter = new ListGambarPresenterImp(this, ListGambar.this, factory);
        mPresenter.getData();
    }

    private void setUi() {
        listGAmbar = findViewById(R.id.main_list_utama);
        parentLayout = findViewById(R.id.maint_parent);
        progressBar = findViewById(R.id.main_progres);
    }

    @Override
    public void addData(ModelGambar modelGambar) {
        listGambarAdapter.addDAta(modelGambar);
    }

    @Override
    public void setDataAdapter(List<ModelGambar> modelGambars) {
        listGambarAdapter = new ListGambarAdapter(modelGambars);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        listGAmbar.setLayoutManager(linearLayoutManager);
        listGAmbar.setAdapter(listGambarAdapter);
    }

    @Override
    public void onError() {
        Commons.getInstance().showSnackbarBottom(parentLayout, "Terjadi Kesalahan").show();
    }

    @Override
    public void onErroMessage(String message) {
        Commons.getInstance().Toast(this, message).show();
    }

    @Override
    public void showProgres() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void closeProgres() {
        progressBar.setVisibility(View.GONE);
    }


}
