package com.fauzan.tesdot.view.activity;

import android.annotation.SuppressLint;
import android.content.Context;

import com.fauzan.tesdot.model.ModelGambar;
import com.fauzan.tesdot.model.ResponGambar;
import com.fauzan.tesdot.service.BaseApi;
import com.fauzan.tesdot.view.util.Commons;

import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;

public class ListGambarPresenterImp implements ListGambarContract.ListGambarPresenter {
    private ListGambarContract.ListGambarView mView;
    private Context context;
    private BaseApi baseApi;
    private CompositeDisposable mDisposable;
    List<ModelGambar> modelGambars = new ArrayList<>();
    List<ModelGambar.Media> mediaList;
    int kali = 0;


    public ListGambarPresenterImp(ListGambarContract.ListGambarView mView, Context context, BaseApi baseApi) {
        this.mView = mView;
        this.context = context;
        this.baseApi = baseApi;
    }


    @Override
    public void getData() {
        if (Commons.getInstance().isConnection(context)) {
            mView.showProgres();
            loadDataImage();
        } else {
            mView.onErroMessage("Tidak Ada Koneksi Internet");
            mView.closeProgres();
        }

    }


    @SuppressLint("CheckResult")
    private void loadDataImage() {
        mView.setDataAdapter(modelGambars);
        mDisposable = new CompositeDisposable();
        baseApi.getGambar().observeOn(AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io()).subscribeWith(new DisposableObserver<ResponGambar>() {
            @Override
            public void onNext(ResponGambar responGambar) {
                if (responGambar != null && responGambar.statusCode == 200) {
                    ModelGambar.Media media = null;

                    for (int i = 0; i < responGambar.data.size(); i++) {
                        mediaList = new ArrayList<>();
                        for (int j = 0; j < responGambar.data.get(i).media.size(); j++) {

                            media = new ModelGambar.Media(responGambar.data.get(i).media.get(j));
                            mediaList.add(media);

                        }

                        ModelGambar modelGambar = new ModelGambar(responGambar.data.get(i).id, responGambar.data.get(i).title, responGambar.data.get(i).content, responGambar.data.get(i).type, mediaList);
                        mView.addData(modelGambar);


                    }
                }
            }

            @Override
            public void onError(Throwable e) {
                if (e instanceof SocketTimeoutException) {
                    if (kali == 0 && Commons.getInstance().isConnection(context)) {
                        kali += 1;
                        loadDataImage();

                    } else {

                        e.printStackTrace();
                        mView.onErroMessage("Periksa Jaringan Internet Anda");
                        kali = 0;
                    }

                } else {
                    mView.onError();
                }
                mView.closeProgres();
            }

            @Override
            public void onComplete() {
                mView.closeProgres();
            }
        });
    }
}
