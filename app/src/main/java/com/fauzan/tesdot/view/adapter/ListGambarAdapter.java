package com.fauzan.tesdot.view.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.fauzan.tesdot.R;
import com.fauzan.tesdot.model.ModelGambar;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ListGambarAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<ModelGambar> modelGambars;
    Context context;
    private final static int TIPE1 = 1;
    private final static int TIPE2 = 2;
    MediaAdapter mediaAdapter;

    public ListGambarAdapter(List<ModelGambar> modelGambars) {
        this.modelGambars = modelGambars;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        switch (viewType) {
            case TIPE1:
            default:
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_media, parent, false);
                return new MediaViewHolder(view);

            case TIPE2:
                View view1 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_multi_media, parent, false);
                return new MultiMediaViewHolder(view1);


        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {
        int tipe = getItemViewType(position);
        switch (tipe) {
            case TIPE1:

                final MediaViewHolder mediaViewHolder = (MediaViewHolder) viewHolder;
                mediaViewHolder.title.setText(modelGambars.get(position).getTitle());
                mediaViewHolder.content.setText(modelGambars.get(position).getContent());
                Picasso.get().load(modelGambars.get(position).getMedia().get(0).getUrl()).into(mediaViewHolder.image);
                break;
            case TIPE2:
                final MultiMediaViewHolder multiMediaViewHolder = (MultiMediaViewHolder) viewHolder;
                multiMediaViewHolder.multititle.setText(modelGambars.get(position).getTitle());
                multiMediaViewHolder.multicontent.setText(modelGambars.get(position).getContent());
                mediaAdapter = new MediaAdapter(modelGambars.get(position).getMedia());
                LinearLayoutManager linearLayoutManager= new LinearLayoutManager(context,LinearLayoutManager.HORIZONTAL, false);
                multiMediaViewHolder.listMedia.setLayoutManager(linearLayoutManager);
                multiMediaViewHolder.listMedia.setAdapter(mediaAdapter);
                break;
        }
    }


    @Override
    public int getItemCount() {
        return modelGambars.size();
    }

    public void addDAta(ModelGambar modelGambar) {
        modelGambars.add(modelGambar);
        notifyItemInserted(modelGambars.size());
    }

    public class MediaViewHolder extends RecyclerView.ViewHolder {
        TextView content, title;
        ImageView image;

        public MediaViewHolder(View itemView) {
            super(itemView);
            content = itemView.findViewById(R.id.item_media_content);
            image = itemView.findViewById(R.id.item_media_gambar);
            title = itemView.findViewById(R.id.item_media_titlle);

        }
    }

    public class MultiMediaViewHolder extends RecyclerView.ViewHolder {
        TextView multicontent, multititle;
        RecyclerView listMedia;

        public MultiMediaViewHolder(View itemView) {
            super(itemView);
            multicontent = itemView.findViewById(R.id.item_multi_content);
            listMedia = itemView.findViewById(R.id.item_multi_recyler);
            multititle = itemView.findViewById(R.id.item_multi_title);

        }


    }

    @Override
    public int getItemViewType(int position) {
        int tipe;

        if (modelGambars.get(position).getType().equals("multiple")) {
            tipe = 2;
        } else tipe = 1;

        return tipe;

    }

}
