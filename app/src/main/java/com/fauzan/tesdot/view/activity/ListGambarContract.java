package com.fauzan.tesdot.view.activity;

import com.fauzan.tesdot.model.ModelGambar;

import java.util.List;

public interface ListGambarContract {
    interface ListGambarView {
        void addData(ModelGambar modelGambar);

        void setDataAdapter(List<ModelGambar> modelGambars);

        void onError();

        void onErroMessage(String message);

        void showProgres();

        void closeProgres();
    }

    interface ListGambarPresenter {
        void getData();
    }
}
