package com.fauzan.tesdot.service;


import com.fauzan.tesdot.model.ResponGambar;
import java.util.concurrent.TimeUnit;
import io.reactivex.Observable;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;

public interface BaseApi {
    @GET("yoesuv/0c274f3314cefd40f66e6ed83f08acc6/raw/8b0c6eb6a95cde6db904f5a0eddba280aef96680/ListData.json")
    Observable<ResponGambar> getGambar();


    class Factory {


        protected static OkHttpClient getNewHttpClient() {
            OkHttpClient.Builder client = new OkHttpClient.Builder()
                    .followRedirects(true)
                    .followSslRedirects(true)
                    .retryOnConnectionFailure(true)
                    .cache(null)
                    .connectTimeout(5, TimeUnit.SECONDS)
                    .writeTimeout(5, TimeUnit.SECONDS)
                    .readTimeout(5, TimeUnit.SECONDS);

            return client.build();
        }

        public static BaseApi create() {

            OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
            clientBuilder.connectTimeout(5, TimeUnit.SECONDS)
                    .readTimeout(5, TimeUnit.SECONDS)
                    .writeTimeout(5, TimeUnit.SECONDS)
                    .retryOnConnectionFailure(true)
                    .build();


            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl("https://gist.githubusercontent.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .client(getNewHttpClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();


            return retrofit.create(BaseApi.class);
        }

    }
}
