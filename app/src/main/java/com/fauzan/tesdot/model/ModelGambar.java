package com.fauzan.tesdot.model;

import java.util.List;

public class ModelGambar {
    private Integer id;
    private String title;
    private String content;
    private String type;
    private List<Media> media;

    public ModelGambar(Integer id, String title, String content, String type, List<Media> media) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.type = type;
        this.media = media;
    }

    public static class Media {
        private String url;

        public Media(String url) {
            this.url = url;
        }

        public String getUrl() {
            return url;
        }
    }

    public Integer getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getType() {
        return type;
    }

    public List<Media> getMedia() {
        return media;
    }
}
