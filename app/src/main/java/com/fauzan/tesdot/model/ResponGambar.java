package com.fauzan.tesdot.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponGambar {
    @SerializedName("status_code")

    public Integer statusCode;
    @SerializedName("data")

    public List<Data> data = null;

    public class Data {

        @SerializedName("id")

        public Integer id;
        @SerializedName("title")

        public String title;
        @SerializedName("content")

        public String content;
        @SerializedName("type")

        public String type;
        @SerializedName("media")

        public List<String> media = null;

    }
}
